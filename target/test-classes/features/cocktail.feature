# language: fr
  ## En français pour l'équipe de dev qui bosse dessus

Fonctionnalité: Commande de Cocktail
  En tant que Romeo, je veut offrir un verre à une personne, afin de discuter ensemble (et plus encore).

  # Equivalent du @Before de jUnit
  Contexte:
  Etant donné que Romeo veut acheter une boisson

  Scénario: Créer une commande vide
  Quand une commande est pour <to>
  Alors il y as 0 cocktails dans la commande.

  Plan du Scénario: Envoyer un message avec une commande
    Quand une commande est pour <to>
    Et un message qui dit "<message>" est ajouté
    Alors le message doit dire "<expected>"

    Exemples:
      | to       | message     | expected                            |
      | Juliette | Wanna chat? | De Romeo pour Juliette: Wanna chat? |
      | Jerry    | Hei!        | De Romeo pour Jerry: Hei!           |