package dojo;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

// Launcher class for Cucumber acceptation's test correspond to the feature file
@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources", monochrome = true
        , plugin = {"pretty", "json:target/site/cucumber-report.json"})
public class RunCucumberTest {
}
