package dojo;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.List;

import static org.junit.Assert.assertEquals;

// Test d'acceptation calqué sur les scénarios du fichier feature
public class CocktailSteps {

    private Order order;

    @Given("{word} veut acheter une boisson")
    public void romeo_who_want_to_buy_a_drink(String name) {
        order = new Order();
        order.declareOwner(name);
    }

    @When("une commande est pour {word}")
    public void an_order_is_declared_for_a_person(String receiver) {
        order.declareTarget(receiver);
    }

    @Then("il y as {int} cocktails dans la commande.")
    public void there_is_no_drink_in_the_order(int nbCocktails) {
        List<String> cocktails = order.getCocktails();
        assertEquals(nbCocktails, cocktails.size());
    }

    @When("un message qui dit {string} est ajouté")
    public void a_message_saying_is_added(String something) {
        order.withMessage(something);
    }
    
    @Then("le message doit dire {string}")
    public void the_ticket_must_say(String somethingElse) {
        assertEquals(somethingElse, order.getTicketMessage());
    }

}
